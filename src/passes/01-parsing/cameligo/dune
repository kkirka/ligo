;; --------------------------------------------------------------------
;; LEXING

;; Build of the lexer

(ocamllex LexToken)

;; Build of the lexer as a library

(library
  (name lexer_cameligo)
  (public_name ligo.lexer.cameligo)
  (modules LexToken)
  (libraries
    ;; Ligo
    lexer_shared
    ;; Third party
    hex)
  (preprocess
    (pps bisect_ppx --conditional)))

;; Build of a standalone lexer

(executable
  (name LexerMain)
  (libraries
    ;; Ligo
    lexer_shared
    lexer_cameligo
    ;; Third party
    hex)
  (modules LexerMain)
  (preprocess
    (pps bisect_ppx --conditional)))

;; --------------------------------------------------------------------
;; PARSING

;; Build of the parser

(menhir
  (merge_into Parser)
  (modules ParToken Parser)
  (flags -la 1 --table --strict --explain
         --external-tokens Lexer_cameligo.LexToken))

;; Build of the parser as a library

(library
  (name parser_cameligo)
  (public_name ligo.parser.cameligo)
  (modules
    Scoping Parser ParErr Pretty)
  (libraries
    ;; Ligo
    lexer_cameligo
    parser_shared
    cst
    ;; Vendors
    simple-utils
    ;; Third party
    pprint
    terminal_size
    menhirLib
    hex)
  (preprocess
    (pps bisect_ppx --conditional))
  (flags (:standard -open Cst_cameligo))) ;; For CST in Parser.mli

;; Build of the unlexer (for covering the
;; error states of the LR automaton)

(executable
  (name Unlexer)
  (libraries str)
  (modules Unlexer)
  (preprocess
    (pps bisect_ppx --conditional)))

;; Local build of a standalone parser

(executable
  (name ParserMain)
  (libraries
    ;; Ligo
    parser_shared
    parser_cameligo
    cst
    ;; Third party
    hex)
  (modules ParserMain Parser_msg)
  (preprocess
    (pps bisect_ppx --conditional)))

;; Build of the covering of error states in the LR automaton

(rule
  (targets Parser.msg)
  (deps (:script_messages ../../../../vendors/ligo-utils/simple-utils/messages.sh) Parser.mly LexToken.mli ParToken.mly)
  (action (run %{script_messages} --lex-tokens=LexToken.mli --par-tokens=ParToken.mly Parser.mly)))

(rule
 (targets Parser_msg.ml)
 (deps Parser.mly ParToken.mly Parser.msg)
 (action
  (with-stdout-to %{targets}
   (bash
     "menhir \
       --compile-errors Parser.msg \
       --external-tokens LexToken \
       --base Parser \
       ParToken.mly \
       Parser.mly"))))

;; Build of all the LIGO source file that cover all error states

(rule
  (targets all.mligo)
  (deps (:script_cover ../../../../vendors/ligo-utils/simple-utils/cover.sh) Parser.mly LexToken.mli ParToken.mly Parser.msg Unlexer.exe)
  (action (run %{script_cover}
               --lex-tokens=LexToken.mli
               --par-tokens=ParToken.mly
               --ext=mligo
               --unlexer=./Unlexer.exe
               --messages=Parser.msg
               --dir=.
               --concatenate Parser.mly)))

;; Error messages

(rule
 (targets error.messages)
 (mode (promote (until-clean) (only *)))
 (deps Parser.mly ParToken.mly error.messages.checked-in LexToken.mli)
 (action
  (with-stdout-to %{targets}
  (run menhir
       --unused-tokens
       --update-errors error.messages.checked-in
       --table
       --strict
       --external-tokens LexToken.mli
       --base Parser.mly
       ParToken.mly
       Parser.mly))))

(rule
  (target error.messages.new)
  (mode (promote (until-clean) (only *)))
  (action
    (with-stdout-to %{target}
      (run menhir
           --unused-tokens
           --list-errors
           --table
           --strict
           --external-tokens LexToken.mli
           --base Parser.mly
           ParToken.mly
           Parser.mly))))

(alias
  (name runtest)
  (deps error.messages error.messages.new)
  (action
    (run menhir
         --unused-tokens
         --table
         --strict
         --external-tokens LexToken.mli
         --base Parser.mly
         ParToken.mly
         Parser.mly
         --compare-errors error.messages.new
         --compare-errors error.messages)))

(rule
 (targets ParErr.ml)
 (mode (promote (until-clean) (only *)))
 (deps Parser.mly ParToken.mly error.messages.checked-in LexToken.mli)
 (action
  (with-stdout-to %{targets}
  (run menhir
       --unused-tokens
       --table
       --strict
       --external-tokens LexToken.mli
       --base Parser.mly
       ParToken.mly
       Parser.mly
       --compile-errors error.messages.checked-in))))
